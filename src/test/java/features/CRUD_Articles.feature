Feature: CRUD Articles Feature

  Tags in the articles

  @Smoke
  @Regression
  Scenario: As user when you are not logged in you can navigate in the "Global Feed" articles if the article contains tags are displayed within article.
    Given I am on Conduit homepage
    When Enter on a article with tag
    Then Tag is in the article

  @Sanity
  @Regression
  Scenario: As user when you are not logged in you can navigate in the "Global Feed" articles, when select  any tag in "Popular Tags" section should be displayed in a new section.
    Given I am on Conduit homepage
    When Select any popular tag "matrix"
    Then Tag is in all article "matrix"

  @Regression
  Scenario: As user when you are not logged you can navigate in the "Global Feed" articles, when select  any user link name should be displayed all posts raised by the user.
    Given I am on Conduit homepage
    When Select any author tag
    Then Author is in all articles

  @Smoke
  @Sanity
  @Regression
  Scenario: Test
    Given I am on Conduit homepage
    When Enter the Username and Password in sign in
    And Create a new article "Test Ivan Automation" "About Automation" "This framework is just an example to review the manage with selenium, page patter object and cucumber"
    Then Is title and article correct "Test Ivan Automation" "This framework is just an example to review the manage with selenium, page patter object and cucumber"
