package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue = "stepdefinition",
        tags = "@Sanity",
        plugin = {"pretty", "html:cucumber-reports/report.html" },
        monochrome=true
)
public class RunSanityTests {
}