package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.How;
import utilities.Utility;

import java.util.List;

public class DriverHelper {

    WebDriver driver;
    public DriverHelper(WebDriver driver){
        this.driver = driver;
    }

    public WebElement findElement(How how, String value){
        switch (how){
            case ID: return driver.findElement(By.id(value));
            case XPATH: return driver.findElement(By.xpath(value));
            default:return null;
        }
    }

    public List<WebElement> findElements(How how, String value){
        switch (how){
            case ID: return driver.findElements(By.id(value));
            case XPATH: return driver.findElements(By.xpath(value));
            default:return null;
        }
    }

    public boolean waitUntilExistsElement(How how, String value, int time){
        Utility.wait(2);
        for(int second = 0 ; second < time; second++) {
            if(existsElement(how, value))
                return true;
            else{
                Utility.wait(second);
            }
        }
        return false;
    }

    private boolean existsElement(How how, String value){
        try {
            switch (how) {
                case XPATH:
                    driver.findElement(By.xpath(value));
                    return true;
                default: return false;
            }
        }catch (Exception ex){
            return false;
        }
    }
}
