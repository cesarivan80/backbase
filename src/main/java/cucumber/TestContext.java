package cucumber;

public class TestContext {
    public TestContext(){
        scenarioContext = new ScenarioContext();
    }
    private ScenarioContext scenarioContext;

    public ScenarioContext getScenarioContext(){
        return scenarioContext;
    }
}
