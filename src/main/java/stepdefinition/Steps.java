package stepdefinition;

import cucumber.ScenarioContext;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.*;

public class Steps extends BaseSteps {
    public Steps(){
       super();
    }
    private WebDriver driver;
    @Given("^I am on Conduit homepage")
    public void open_the_chrome_and_launch_the_application()
    {
        driver = getDriver();
        driver.get(getUrl());
    }

    //When
    @When("^Enter the Username and Password in sign in")
    public void login_to_conduit() throws Throwable
    {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.login(getUserName(), getEmail(), getPass());
    }

    @When("^Enter on a article with tag")
    public void find_article_with_tag()
    {
        GlobalFeedPage globalFeedPage = PageFactory.initElements(driver, GlobalFeedPage.class);
        testContext.getScenarioContext().setContext(ScenarioContext.Context.TAG, globalFeedPage.findClickAndGetTagText());
    }

    @When("^Select any popular tag \"([^\"]*)\"$")
    public void find_articles_with_popular_tag(String tag)
    {
        GlobalFeedPage globalFeedPage = PageFactory.initElements(driver, GlobalFeedPage.class);
        globalFeedPage.selectPopularTag(tag);
    }

    @When("^Select any author tag")
    public void find_author_in_articles()
    {
        GlobalFeedPage globalFeedPage = PageFactory.initElements(driver, GlobalFeedPage.class);
        globalFeedPage.selectFirstAuthor();
        testContext.getScenarioContext().setContext(ScenarioContext.Context.AUTHOR, globalFeedPage.selectFirstAuthor());
    }

    @When("^Create a new article \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void create_new_article(String title, String aboutArticle, String article)
    {
        YourFeedPage yourFeedPage = PageFactory.initElements(driver, YourFeedPage.class);
        NewArticlePage newArticlePage = yourFeedPage.clickOnNewArticleLink();
        newArticlePage.register(title, aboutArticle, article);
    }

    @Then("^Tag is in the article")
    public void tag_is_in_article()
    {
        ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
        Assert.assertEquals("", true, articlePage.containsTag((String) testContext.getScenarioContext().getContext(ScenarioContext.Context.TAG)));
    }

    @Then("^Tag is in all article \"([^\"]*)\"$")
    public void tag_is_in_all_article(String tag)
    {
        PopularTagPage popularTagPage = PageFactory.initElements(driver, PopularTagPage.class);
        Assert.assertEquals("", true, popularTagPage.allArticlesContainsTag(tag));
    }

    @Then("^Author is in all articles")
    public void author_is_in_all_articles()
    {
        UserPage userPage = PageFactory.initElements(driver, UserPage.class);
        Assert.assertEquals("", true, userPage.authorIs((String) testContext.getScenarioContext().getContext(ScenarioContext.Context.AUTHOR)));
    }

    @Then("^Is title and article correct \"([^\"]*)\" \"([^\"]*)\"$")
    public void is_title_and_article_correct(String title, String article)
    {
        ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
        Assert.assertEquals("", true, articlePage.isTheInfoCorrect(title, article));
    }

    @After
    public void afterScenario()
    {
        driver.quit();
    }
}
