package stepdefinition;

import cucumber.TestContext;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class BaseSteps {
    protected Properties properties;
    protected TestContext testContext;
    public BaseSteps(){
        properties = new Properties();
        try {
            properties.load(new FileInputStream("src/test/resources/test.properties"));
            testContext = new TestContext();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected String getUrl(){
        return (String) properties.get("url");
    }

    protected String getEmail(){
        return (String) properties.get("email");
    }

    protected String getPass(){
        return (String) properties.get("pass");
    }

    protected String getUserName(){
        return (String) properties.get("username");
    }

    protected WebDriver getDriver(){
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.addArguments("disable-infobars");
        chromeOptions.addArguments("--ignore-ssl-errors=yes");
        chromeOptions.addArguments("--ignore-certificate-errors");
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver(chromeOptions);
    }
}
