package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends CommonPage{
    public RegisterPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = ".//input[@placeholder= 'Username']")
    private WebElement userNameTextField;
    @FindBy(xpath = ".//input[@placeholder= 'Email']")
    private WebElement emailTextfield;
    @FindBy(xpath = ".//input[@placeholder= 'Password']")
    private WebElement passwordTextField;
    @FindBy(xpath = ".//button[contains(text(),'Sign up')]")
    private WebElement signUpButton;
    @FindBy(xpath = ".//ul[@class='error-messages']//li")
    private WebElement errorMessageLabel;

    public void register(String username, String email, String password){
        elementHelper.sendKeys(userNameTextField, username);
        elementHelper.sendKeys(emailTextfield, email);
        elementHelper.sendKeys(passwordTextField, password);
        elementHelper.click(signUpButton);
    }
}
