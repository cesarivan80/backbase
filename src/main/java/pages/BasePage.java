package pages;

import helpers.DriverHelper;
import helpers.ElementHelper;
import org.openqa.selenium.WebDriver;

public abstract class BasePage {
    protected WebDriver driver;
    protected ElementHelper elementHelper;
    protected DriverHelper driverHelper;

    public BasePage(WebDriver driver){
        this.driver = driver;
        elementHelper = new ElementHelper(driver);
        driverHelper = new DriverHelper(driver);
    }
}
