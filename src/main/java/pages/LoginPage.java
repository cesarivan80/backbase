package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends CommonPage{
    public LoginPage(WebDriver driver) {
        super(driver);
    }
    private final String errorMessageXpath = ".//ul[@class='error-messages']//li";
    @FindBy(xpath = ".//input[@placeholder= 'Email']")
    private WebElement userNameTextField;
    @FindBy(xpath = ".//input[@placeholder= 'Password']")
    private WebElement passwordTextField;
    @FindBy(xpath = ".//button[contains(text(),'Sign in')]")
    private WebElement signInButton;
    @FindBy(xpath = ".//ul[@class='error-messages']//li")
    private WebElement errorMessageLabel;

    public GlobalFeedPage login(String username, String email,  String password){
        clickOnSignInLink();
        elementHelper.sendKeys(userNameTextField, email);
        elementHelper.sendKeys(passwordTextField, password);
        elementHelper.click(signInButton);
        if(driverHelper.waitUntilExistsElement(How.XPATH, errorMessageXpath, 3) && elementHelper.getText(errorMessageLabel).trim().equalsIgnoreCase("email or password is invalid")){
            clickOnSignUpLink();
            RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
            registerPage.register(username, email, password);
        }
        return PageFactory.initElements(driver, GlobalFeedPage.class);
    }
}
