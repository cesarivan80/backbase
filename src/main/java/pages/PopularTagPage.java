package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class PopularTagPage extends SearchPage{
    public PopularTagPage(WebDriver driver) {
        super(driver);
    }
    private final String tagsByArticleXpath = "(.//ul[@class='tag-list'])[%s]//li";
    private final String articlesListXpath = ".//div[@class='article-preview']";
    private final String loadingArticlesXpath = ".//div[@class='app-article-preview' and @hidden and normalize-space(text()) = 'Loading articles...']";
    @FindAll(@FindBy(xpath = ".//li[@class = 'page-item active' or @class='page-item']/a[@class='page-link']"))
    private List<WebElement> pageList;

    public boolean allArticlesContainsTag(String tag) {
        waitUntilLoadingArticles();
        boolean exists = false;
        int pageIndex = 1;
        do {
                List<WebElement> articlesList = driverHelper.findElements(How.XPATH, articlesListXpath);
                for (int index = 1; index <= articlesList.size(); index++) {
                    List<WebElement> tagArticleList = driverHelper.findElements(How.XPATH, String.format(tagsByArticleXpath, index));
                    exists = false;
                    for (WebElement tagArticle : tagArticleList) {
                        if (elementHelper.getText(tagArticle).equalsIgnoreCase(tag)) {
                            exists = true;
                            System.out.println(elementHelper.getText(tagArticle));
                            break;
                        }
                    }
                    if (!exists)
                        return exists;
                }
                if (pageList.size() > 1) {
                    elementHelper.click(pageList.get(pageIndex - 1));
                    if(!waitUntilLoadingArticles())
                        return false;
                }
                pageIndex++;
            }
            while (pageIndex < pageList.size()) ;
        return exists;
    }
}
