package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ArticlePage extends CommonPage{
    public ArticlePage(WebDriver driver) {
        super(driver);
    }
    @FindAll(@FindBy(xpath = ".//li[@class = 'tag-default tag-pill tag-outline']"))
    private List<WebElement> articlesList;
    @FindBy(xpath = ".//div[@class='banner']//h1")
    private WebElement titleText;
    @FindBy(xpath = ".//div[@class='row article-content']//p")
    private WebElement articleText;

    public boolean containsTag(String tagText){
        for(WebElement article : articlesList) {
            if(article.getText().equalsIgnoreCase(tagText))
                return true;
        }
        return false;
    }

    public boolean isTheInfoCorrect(String title, String article){
        if(elementHelper.getText(titleText).trim().equalsIgnoreCase(title) && elementHelper.getText(articleText).trim().equalsIgnoreCase(article))
            return true;
        return false;
    }
}
