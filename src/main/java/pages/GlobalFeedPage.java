package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class GlobalFeedPage extends SearchPage{
    public GlobalFeedPage(WebDriver driver) {
        super(driver);
    }
    private final String articleWithTagXpath = ".//div[@class='article-preview']//li[@class='tag-default tag-pill tag-outline']";
    @FindAll(@FindBy(xpath = ".//div[@class='article-preview']"))
    private List<WebElement> articlesList;
    @FindAll(@FindBy(xpath = ".//li[@class = 'page-item active' or @class='page-item']/a[@class='page-link']"))
    private List<WebElement> pageList;

    public String findClickAndGetTagText(){
        waitUntilLoadingArticles();
        for(WebElement page : pageList) {
            List<WebElement> articleWithTagList = driverHelper.findElements(How.XPATH, articleWithTagXpath);
            if(articleWithTagList.size() > 0){
                String tag = articleWithTagList.get(0).getText();
                elementHelper.click(articleWithTagList.get(0));
                return tag;
            }
            elementHelper.click(page);
            waitUntilLoadingArticles();
        }
        return null;
    }
}
