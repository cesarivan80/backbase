package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public abstract class CommonPage extends BasePage{
    public CommonPage(WebDriver driver) {
        super(driver);
    }

    private final String newTabItemXpath = ".//div[@class='feed-toggle']//li[@class = 'nav-item']//i";
    @FindBy(xpath = ".//a[@href = '/login']")
    private WebElement signinLink;
    @FindBy(xpath = ".//a[@href = '/register']")
    private WebElement signUpLink;
    @FindBy(xpath = ".//a[@href = '/editor']")
    private WebElement newArticleLink;
    @FindAll(@FindBy(xpath = ".//a[@class = 'tag-default tag-pill']"))
    private List<WebElement> populartagsList;
    @FindAll(@FindBy(xpath = ".//a[@class='author']"))
    private List<WebElement> authorList;

    public void clickOnSignInLink(){
        elementHelper.click(signinLink);
    }

    public void clickOnSignUpLink(){
        elementHelper.click(signUpLink);
    }

    public NewArticlePage clickOnNewArticleLink(){
        elementHelper.click(newArticleLink);
        return PageFactory.initElements(driver, NewArticlePage.class);
    }

    public void selectPopularTag(String tag){
        for (WebElement popularTag : populartagsList){
            if(popularTag.getText().trim().equalsIgnoreCase(tag)){
                elementHelper.click(popularTag);
                driverHelper.waitUntilExistsElement(How.XPATH, newTabItemXpath, 60);
                break;
            }
        }
    }

    public String selectFirstAuthor(){
        String author = elementHelper.getText(authorList.get(0));
        elementHelper.click(authorList.get(0));
        return author;
    }
}
