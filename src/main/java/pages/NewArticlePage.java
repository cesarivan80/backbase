package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NewArticlePage extends CommonPage{
    public NewArticlePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = ".//input[@placeholder= 'Article Title']")
    private WebElement titleTextField;
    @FindBy(xpath = ".//input[contains(@placeholder,'article about')]")
    private WebElement whatThisArticleAboutTextfield;
    @FindBy(xpath = ".//textarea[contains(@placeholder,'Write your article')]")
    private WebElement writeYourArticleTextArea;
    @FindBy(xpath = ".//button[contains(text(),'Publish Article')]")
    private WebElement publishArticleButton;

    public ArticlePage register(String title, String aboutArticle, String article){
        elementHelper.sendKeys(titleTextField, title);
        elementHelper.sendKeys(whatThisArticleAboutTextfield, aboutArticle);
        elementHelper.sendKeys(writeYourArticleTextArea, article);
        elementHelper.click(publishArticleButton);
        return PageFactory.initElements(driver, ArticlePage.class);
    }
}
