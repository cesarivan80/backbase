package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class UserPage extends CommonPage{
    public UserPage(WebDriver driver) {
        super(driver);
    }
    private final String authorLinkXpath = ".//a[@class='author']";
    private final String loadingArticlesXpath = ".//div[@class='app-article-preview' and @hidden and normalize-space(text()) = 'Loading articles...']";
    private final String followAuthorXpath = ".//button[@class='btn btn-sm action-btn btn-outline-secondary']";
    @FindAll(@FindBy(xpath = ".//li[@class = 'page-item active' or @class='page-item']/a[@class='page-link']"))
    private List<WebElement> pageList;

    public boolean authorIs(String authorName) {
        if (!driverHelper.waitUntilExistsElement(How.XPATH, loadingArticlesXpath, 60))
            return false;
        //Utility.wait(5);
        int pageIndex = 1;
        do {
                List<WebElement> authorList = driverHelper.findElements(How.XPATH, authorLinkXpath);
                for (WebElement author : authorList) {
                    if (!elementHelper.getText(author).equalsIgnoreCase(authorName))
                        return false;
                }
                if (pageList.size() > 1) {
                    elementHelper.click(pageList.get(pageIndex - 1));
                    if(!driverHelper.waitUntilExistsElement(How.XPATH, loadingArticlesXpath, 60))
                       return false;
                }
                pageIndex++;
            }
            while (pageIndex < pageList.size()) ;
        return true;
    }
}
