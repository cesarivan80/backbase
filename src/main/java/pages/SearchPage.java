package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.How;

public abstract class SearchPage extends CommonPage{
    public SearchPage(WebDriver driver) {
        super(driver);
    }
    private final String loadingArticlesXpath = ".//div[@class='app-article-preview' and normalize-space(text()) = 'Loading articles...']";
    private final String loadingArticlesHiddenXpath = ".//div[@class='app-article-preview' and @hidden and normalize-space(text()) = 'Loading articles...']";

    protected boolean waitUntilLoadingArticles(){
        driverHelper.waitUntilExistsElement(How.XPATH, loadingArticlesXpath, 60);
        if (!driverHelper.waitUntilExistsElement(How.XPATH, loadingArticlesHiddenXpath, 60))
            return false;
        return true;
    }
}
